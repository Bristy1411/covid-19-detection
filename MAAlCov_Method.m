% Clear workspace
clear,clc, close all
% Images Datapath – You can modify your path accordingly 
folder = fullfile('TestDataSet');
datapath = fullfile(folder, 'Train');
trainfolder = {'CovidNegative', 'CovidPositive'};
 
% Image Datastore
imds=imageDatastore(fullfile(datapath, trainfolder), 'LabelSource','foldernames'); %Datastore for image data

All_samples = countEachLabel(imds); %Count files in ImageDatastore labels

% Number of Images
num_images=length(imds.Labels);

% Visualize random images
randper=randperm(num_images,12); %Random permutation of integers
figure;
for idx=1:length(randper)
    
    subplot(3,4,idx); %Create axes in tiled positions
    imshow(imread(imds.Files{randper(idx)}));
    title((imds.Labels(randper(idx))))
end

[imdsTrain, imdsTest] = splitEachLabel(imds,0.9,'randomized'); %Split ImageDatastore labels by proportions out of 40images 30 are using for training

%Load RESNet51
net=resnet50;
    lgraph = layerGraph(net); % Extract all layers
        lgraph.Layers % visualize all layers
    lgraph.Layers(175).OutputSize % get number of classes in resnet-50
    clear net;
    
%count number of categories
numClasses = numel(categories(imdsTrain.Labels)); %Number of array elements
fprintf("Number of Classes: %d\n",numClasses);

%create a new layer
newLearnableLayer = fullyConnectedLayer(numClasses, ...
        'Name','new_fc', ...
        'WeightLearnRateFactor',10, ...
        'BiasLearnRateFactor',10);

%replace last three layers
lgraph = replaceLayer(lgraph,'fc1000',newLearnableLayer);
%A softmax layer applies a softmax function to the input.
newsoftmaxLayer = softmaxLayer('Name','new_softmax');
lgraph = replaceLayer(lgraph,'fc1000_softmax',newsoftmaxLayer);
%A classification layer computes the cross entropy loss for multi-class
%  classification problems with mutually exclusive classes.
newClassLayer = classificationLayer('Name','new_classoutput');
lgraph = replaceLayer(lgraph,'ClassificationLayer_fc1000',newClassLayer);

% visualize last 3 layers after updating
lgraph.Layers(end-2)
lgraph.Layers(end-1)
lgraph.Layers(end)

%view layer architecture
figure,
    plot(lgraph)

imageSize = lgraph.Layers(1).InputSize;

%gray to RGB conversion

imdsTrain.ReadFcn = @(trainfolder)preprocess_images(trainfolder);
imdsTest.ReadFcn = @(trainfolder)preprocess_images(trainfolder);

%Trainig Option
options = trainingOptions('adam',...
        'MaxEpochs',5,'MiniBatchSize',20,...
        'Shuffle','every-epoch', ...
        'InitialLearnRate',1e-4, ...
        'Verbose',false, ...
        'Plots','training-progress'); %Options for training deep learning neural network

%Data Augmentation
augmenter = imageDataAugmenter( ...
        'RandRotation',[-5 5],'RandXReflection',1,...
        'RandYReflection',1,'RandXShear',[-0.05 0.05],'RandYShear',[-0.05 0.05]);

%Resizing all training images to [224 224] for ResNet architecture
auimds = augmentedImageDatastore([224 224],imdsTrain,'DataAugmentation',augmenter); 

%metrics Resize

%training part
netTransfer = trainNetwork(auimds,lgraph,options);

%testing
augtestimds = augmentedImageDatastore([224 224],imdsTest);
predicted_labels = classify(netTransfer,augtestimds);

%accuracy
accuracy = sum(predicted_labels==imdsTest.Labels)/numel(predicted_labels)*100;
display(accuracy);

function IMout = preprocess_images(trainfolder)

I = imread(trainfolder);

if ~ismatrix(I) % if matrix is not in form m*n
    I=rgb2gray(I); 
end
IMout = cat(3,I,I,I); % creating matrix in the form of m*n*3
end

